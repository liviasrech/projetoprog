<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ordenar Números</title>
    <style>
    
    h1{
    font-family: arial, sans-serif;
    font-weight: bold;
    text-align: center;
    background-color: #1fad4e;
    }

    table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    font-size: 30px;
    width: 80%;
    text-align: center;
    }
  
    td {
    border: 3px solid black;
    padding: 8px;
    }

    #par{
        color: red;
    }

    #impar{
        color: blue;
    }
    </style>
</head>
<body>
        <h1> Ordenar números</h1>

        <center>
        <table>
        <?php
            //código
            $numeros = array();
            $pares = 0;
            $impares = 0;
            for ($i=0; $i < 100 ; $i++) { 
                $numeroA = rand(0,100);
                $numeros[$i] = $numeroA;
                if($numeroA%2==0){
                    $pares++;
                }else{
                    $impares++;
                }  
            }

            if($pares>$impares){
                sort($numeros);
            }else{
                rsort($numeros);
            }

            echo "<tr>";
                for ($i=0; $i<100; $i++) {
                    if ($i%10==0 && $i!=0) {
                        echo "</tr>";
                        echo "<tr>";
                    }
                    if ($numeros[$i]%2==0) {
                        echo "<td id='par'>".$numeros[$i]."</td>";
                    }else {
                        echo "<td id='impar'>".$numeros[$i]."</td>";
                    }
                }
        ?>
        </center>
</body>
</html>



